package OpenAPI::Generator;

use Moose;
use namespace::autoclean;

#ABSTRACT: An code generator for controllers and routes based on OpenAPI specs

use JSON::XS qw(decode_json);
use YAML::XS qw(LoadFile);
use IO::All;

has path => (
    is => 'ro',
    isa => 'Str',
    required => 1,
);

has data => (
    is => 'ro',
    isa => 'HashRef',
    lazy => 1,
    builder => 'parse_file',
);

has routes => (
    is => 'ro',
    isa => 'ArrayRef',
    traits => ['Array'],
    default => sub { [] },
    handles => {
        add_route => 'push',
    },
);

sub parse_file {
    my $self = shift;

    my $ext = lc((split(/\./, $self->path))[-1]);
    if ($ext eq 'json') {
        return decode_json(io->catfile($self->path)->slurp);
    }
    else {
        return LoadFile($self->path);
    }
}

sub get_paths {
    my $self = shift;

    my $paths = $self->data->{paths};

    foreach my $p (sort keys %{$paths}) {
        my $methods = $paths->{$p};
        foreach my $m (sort keys %{$methods}) {
            my $route = $self->get_method($m, $methods->{$m});
            $route->{uri} = $p;
            $self->add_route($route)
        }
    }
}

sub parse {
    my $self = shift;

    $self->parse_file;
    $self->get_paths;
    return;
}

sub get_method {
    my ($self, $method_name, $method_definition) = @_;

    my %route = (
        method     => uc($method_name),
        controller => $method_definition->{'x-controller'},
        action     => $method_definition->{operationId},
        map { $_ => $method_definition->{$_} } qw(summary description tags),
    );

    my %params = ();
    foreach my $p (@{$method_definition->{parameters} // []}) {
        my $type = delete $p->{in};
        push(
            @{ $params{$type} },
            { %$p }
        );
    }
    $route{params} = \%params if %params;
    return \%route;
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 DESCRIPTION

Code generation for server files based on OpenAPI specifications

=head1 SYNOPSIS

    use OpenAPI::Generator;

    my $generator = OpenAPI::Generator->new(
        path => '/path/to/openapi.yaml',
    );

    $generator->run;
