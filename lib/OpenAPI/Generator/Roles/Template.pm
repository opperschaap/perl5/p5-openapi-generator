package OpenAPI::Generator::Roles::Template;

use Moose;
use Template;
use Carp;

# ABSTRACT: A templating class/role

has template => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has tt => (
    is => 'ro',
    isa => 'Template',
    lazy => 1,
    builder => '_build_tt',
);

has include_path => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

sub create {
    my ($self, $fh, %args) = @_;

    my $output = '';
    my $self->tt->process->($self->template, \%args, \$output)
        or croak($self->tt->error());

    print {$fh} $output;
    return 1;
}

sub _build_tt {
    my ($self, @args) = @_;

    return Template->new(
        @args,
        INCLUDE_PATH => $self->include_path,
    );
}

1;

__END__

=head1 DESCRIPTION

=head1 SYNOPSIS
