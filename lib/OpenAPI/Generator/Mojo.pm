package OpenAPI::Generator::Mojo;

use Moose;

# ABSTRACT: Generate Mojo controllers and routes based on OpenAPI specs

has routes => (
    is       => 'ro',
    isa      => 'ArrayRef',
    required => 1,
);

around create => sub {
    my $orig = shift;
    my $self = shift;

    my @args = (routes => $self->routes);

    $orig->($self, @args);

};



__PACKAGE__->meta->make_immutable;

__END__

=head1 DESCRIPTION

=head1 SYNOPSIS
