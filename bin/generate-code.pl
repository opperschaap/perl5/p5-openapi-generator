#!/usr/bin/perl
use warnings;
use strict;

# ABSTRACT: Generate server code based on OpenAPI specifications
# PODNAME: generate-code.pl

use Config::Any;
use Data::Dumper;
use File::Spec::Functions qw(catfile);
use Getopt::Long;
use OpenAPI::Generator;
use Pod::Usage;

my %opts = (
    help => 0,
    config => catfile($ENV{HOME}, qw (.config openapi-generator config.conf))
);


{
    local $SIG{__WARN__};
    my $ok = eval {
        GetOptions(
            \%opts, qw(help secret=s endpoint=s sender=s
                document-package-id=s file=s@ signer=s@ use-fh)
        );
    };
    if (!$ok) {
        die($@);
    }
}

pod2usage(0) if ($opts{help});

if (-f $opts{config}) {
    my $config = Config::Any->load_files({
            files => [$opts{config}],
            use_ext => 1,
            flatten_hash => 1,

        })->[0]{$opts{config}};

    foreach (keys %$config) {
        # If an option is set multiple times in the config file, take
        # the last value and work with that
        $opts{$_} ||= ref $config->{$_} eq 'ARRAY'
            ? $config->{$_}[-1]
            : $config->{$_};
    }
}

my @required = qw(openapi type controller);
my @missing;
foreach (@required) {
    if (!defined $opts{$_}) {
        push(@missing, $_);
    }
}

if (@missing) {
    warn "One or more options are missing: " . join(", ", @missing) . "\n\n";
    pod2usage(1);
}

my $file       = $ARGV[0];
my $type       = $ARGV[1];
my $routes     = $ARGV[2];
my $controller = $ARGV[3];

my $generator = OpenAPI::Generator->new(
    path       => $file,
    type       => $type,
    routes     => $routes,
    controller => $controller,
);

$generator->run();

__END__

=head1 SYNOPSIS

generate-code.pl OPTIONS

=head1 DESCRIPTION

An OpenAPI code generator for servers

=head1 OPTIONS

=over

=item openapi

Your OpenAPI specification

=item type

The type of generator you want to use

=item routes

The name of the routes, optional

=item controller

The namespace of your controllers

=item config

Define where your configuration file is located, defaults to
C<$HOME/.config/openapi-generator/config.conf>. In here you can
change the defaults for all of the command line options. Command line
options take preference over the configuration file options.

=back

=head1 SEE ALSO

=over

=item L<Some::Module::On::CPAN>

=back

=cut
