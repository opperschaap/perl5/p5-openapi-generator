# DESCRIPTION

Code generation for server files based on OpenAPI specifications

# SYNOPSIS

    use OpenAPI::Generator;

    my $generator = OpenAPI::Generator->new(
        path => '/path/to/openapi.yaml',
    );

    $generator->run;
